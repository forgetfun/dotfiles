function add_jabba_jdks_to_jenv
	for name in (jabba ls);
		# consider suppressing the output for jdks that were already present
		jenv add (jabba which $name);
	end
end
