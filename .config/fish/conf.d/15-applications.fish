#########
# volta #
#########
set -gx VOLTA_HOME $app_root/volta
set -gxp PATH $VOLTA_HOME/bin

########
# rust #
########
set -gx RUSTUP_HOME $app_root/rustup
set -gx CARGO_HOME $app_root/cargo
set -gxp PATH $CARGO_HOME/bin

#########
# jabba #
#########
set -gx JABBA_HOME $app_root/jabba
source $JABBA_HOME/jabba.fish

########
# jenv #
########
set -g jenv_home $app_root/jenv
set -gxp PATH $jenv_home/bin
if status --is-interactive;
	source (jenv init -|psub)

	# add all jabba jdks to jenv
	# currently disabled because it takes forever.
	# add_jabba_jdks_to_jenv
end

###########
# android #
###########
set -gx ANDROID_SDK_ROOT $app_root/android_sdk
set -gxp PATH $ANDROID_SDK_ROOT/emulator $ANDROID_SDK_ROOT/tools $ANDROID_SDK_ROOT/tools/bin $ANDROID_SDK_ROOT/platform-tools

############
# autojump #
############
#status --is-interactive; and source /usr/share/autojump/autojump.fish

##########
# zoxide #
##########
zoxide init fish | source

###################
# starship prompt #
###################
starship init fish | source

##########
# golang #
##########
set -gx GOPATH ~/go
set -gxp PATH /usr/local/go/bin $GOPATH/bin

##############
# pkg-config #
##############

# for reasons unknown to me, pkg-config only looks in `/usr/share/pkgconfig`, but ignores `/usr/lib/pkgconfig`
set -gxp PKG_CONFIG_PATH /usr/lib/pkgconfig /usr/share/pkgconfig


##########
# direnv #
##########
direnv hook fish | source

###########################
# local scripts/overrides #
###########################
set -gxp PATH ~/.local/bin:/usr/sbin
